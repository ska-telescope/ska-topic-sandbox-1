********************************************************************************
SKA CSP-Low PSI Prototype Infrastructure
********************************************************************************

.. toctree::
  :maxdepth: 2
  :caption: README
  :hidden:

   ../../README

.. important::
  The CSP-Low PSI Prototype Kubernetes cluster is currently no longer running.

================================================================================
Jupyterhub
================================================================================

The CSP-Low PSI Kubernetes cluster contains an instance of Jupyterhub, which is set up to run Jupyter Notebooks that communicate with the TANGO devices on the network.

The Jupyterhub UI is accessible at http://skao0.topic.local/jupyterhub/. The GitLab authentication is set up to allow anyone who is part of the ``ska-telescope`` GitLab group.

.. tip::
  In case ``skao.topic.local`` cannot be resolved, add an entry to ``/etc/hosts`` that points it to the IP address of the ``skao0`` node.
