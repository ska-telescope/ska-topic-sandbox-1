# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html


def setup(app):
    app.add_css_file('css/custom.css')

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information


project = 'ska-cicd-deploy-low-clp'
copyright = '2022, TOPIC Team'
author = 'TOPIC Team'
release = '0.0.1'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    'recommonmark',
    'sphinx.ext.githubpages',
    'sphinx.ext.ifconfig',
    'sphinx.ext.intersphinx',
]

source_suffix = ['.rst', '.md']

templates_path = ['_templates']
exclude_patterns = []

pygments_style = 'sphinx'

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']

html_context = {
    'favicon': 'img/favicon_mono.ico',
    'logo': 'img/logo.png',
    'theme_logo_only': True,
    'display_gitlab': True,
    'gitlab_user': 'ska-telescope',
    'gitlab_repo': project,
    'gitlab_version': 'main',
    'conf_py_path': '/docs/src/',  # Path in the checkout to the docs root
    'theme_vcs_pageview_mode': 'edit',
}
