async def discover_device_servers(self):
    self.profile_list = []

    services = await self.api.list_service_for_all_namespaces(label_selector="component=databaseds")

    for svc in services.items:
        tango_host = f'{svc.metadata.name}.{svc.metadata.namespace}:{svc.spec.ports[0].target_port}'
        self.profile_list.extend([
            {
                'display_name': f'Tango Notebook (namespace: {svc.metadata.namespace})',
                'description': f'A tango notebook configured with TANGO_HOST={tango_host}',
                'kubespawner_override': {
                    'environment': {
                        'TANGO_HOST': tango_host,
                    },
                },
            }
        ])

    return self._options_form_default()

c.KubeSpawner.options_form = discover_device_servers