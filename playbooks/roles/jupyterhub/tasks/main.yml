- name: Add Jupyterhub helm repo
  kubernetes.core.helm_repository:
    name: jupyterhub
    repo_url: https://jupyterhub.github.io/helm-chart/

- name: Deploy Jupyterhub chart
  kubernetes.core.helm:
    name: jupyterhub
    chart_ref: jupyterhub/jupyterhub
    chart_version: "{{ jupyterhub_chart_version }}"
    release_namespace: "{{ jupyterhub_namespace }}"
    create_namespace: true
    values:
      hub:
        baseUrl: /jupyterhub/
        serviceAccount:
          create: true
          # Override the service account name so that we can target it in the ClusterRoleBinding below
          name: "{{ jupyterhub_serviceaccount_name }}"
        config:
          JupyterHub:
            authenticator_class: gitlab
          GitLabOAuthenticator:
            client_id: "{{ gitlab_oauth_client_id }}"
            client_secret: "{{ gitlab_oauth_client_secret }}"
            scope:
              - api
              - read_user
            oauth_callback_url: "{{ k8s_ingress_address }}/jupyterhub/hub/oauth_callback"
            allowed_gitlab_groups:
              - ska-telescope
        extraConfig:
          options_form: "{{ lookup('file', 'scripts/options_form.py') }}"
      ingress:
        enabled: true
        hosts:
          - "{{ k8s_ingress_host }}"
        ingressClassName: nginx
      singleuser:
        image:
          name: registry.gitlab.com/ska-telescope/ska-cicd-deploy-low-clp/tango-notebook
          tag: 0.0.0
        networkPolicy:
          # Disable network policies so that notebooks can access Tango devices in the cluster
          enabled: false

- name: Add Jupyterhub ClusterRole
  kubernetes.core.k8s:
    state: present
    definition:
      kind: ClusterRole
      apiVersion: rbac.authorization.k8s.io/v1
      metadata:
        name: jupyterhub-hub
      rules:
        - apiGroups: [""]
          resources: ["services"]
          verbs: ["get", "list"]

- name: Add Jupyterhub ClusterRoleBinding
  kubernetes.core.k8s:
    state: present
    definition:
      kind: ClusterRoleBinding
      apiVersion: rbac.authorization.k8s.io/v1
      metadata:
        name: jupyterhub-hub
      subjects:
        - kind: ServiceAccount
          name: "{{ jupyterhub_serviceaccount_name }}"
          namespace: "{{ jupyterhub_namespace }}"
      roleRef:
        kind: ClusterRole
        name: jupyterhub-hub
        apiGroup: rbac.authorization.k8s.io
