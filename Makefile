COLLECTIONS_PATH ?= collections
INVENTORY_FILE ?= inventory_clp

OCI_IMAGE = tango-notebook

include .make/base.mk
include .make/oci.mk

# define overrides for above variables in here
-include PrivateRules.mak

.DEFAULT_GOAL := help

vars:  ## Variables
	@echo "Current variable settings:"
	@echo "COLLECTIONS_PATH=$(COLLECTIONS_PATH)"
	@echo "INVENTORY_FILE=$(INVENTORY_FILE) (for node build)"
.PHONY: vars

all: build
.PHONY: all

build: build_clp  ## Build nodes
.PHONY: build

build_clp: patch_playbook  ## Build CLP nodes
	ANSIBLE_COLLECTIONS_PATHS=$(COLLECTIONS_PATH) \
	ansible-playbook \
	  -i $(INVENTORY_FILE) $(V) \
	  playbooks/nodes.yml
.PHONY: build_clp

distribute-keys:  ## update users' SSH keys
	ANSIBLE_ROLES_PATH=playbooks/roles ansible --become -m import_role -a name=ssh-keys \
	  -i $(INVENTORY_FILE) masters

install:  ## Install dependent ansible collections
	if [ -f $(COLLECTIONS_PATH)/.collected ]; then \
	  echo "Already collected !"; \
	else \
	  ansible-galaxy collection install \
	    -r requirements.yml -p ./collections; \
	  ansible-galaxy install -r requirements.yml --roles-path ./roles; \
	  touch $(COLLECTIONS_PATH)/.collected; \
	fi
.PHONY: install

lint:  ## Lint check playbook
	ansible-lint \
	  playbooks/*.yml
.PHONY: lint

patch_playbook: install ## Patch playbook
	ANSIBLE_COLLECTIONS_PATHS=$(COLLECTIONS_PATH) \
	ansible-playbook \
	  -i $(INVENTORY_FILE) $(V) \
	  playbooks/local.yml
.PHONY: patch_playbook

reinstall: uninstall install ## Reinstall collections
.PHONY: reinstall

uninstall:  # Uninstall collections
	rm -rf $(COLLECTIONS_PATH)/ansible_collections/* $(COLLECTIONS_PATH)/.collected
.PHONY: uninstall
