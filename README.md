# SKA CSP-Low PSI Prototype Infrastructure

| :warning: Deprecated                            |
|-------------------------------------------------|
| The CLP prototype cluster is no longer running. |

Infrastructure-as-code for the CLP prototype cluster located in the TOPIC office, The Netherlands.

## Adding a new node

- Install Ubuntu-20.04 server on a machine:
  - The recommended user name is `ubuntu`.
  - Ensure an SSH server is installed on the machine.
- Configure the machine:
  - Copy your public key to the machine so that you can do a password-less login to that machine.

When not using `ubuntu` as the user name, configure a host variable setting `ansible_ssh_user`.

For example:
```
# host_vars/<HOSTNAME>/all
ansible_ssh_user = username
```

## Enabling the runner

In a perfect world, it'll just work.
If it doesn't, confirm that the `gitlab_agent_token` and the `gitlab_runner_registration_token` in `group_vars/all.yml` contain the appropriate values, and then re-apply the `gitlab-runner` role.


## Support

For questions or remarks related to this project, contact the TOPIC Team:

- [Confluence site](https://confluence.skatelescope.org/display/SE/TOPIC+Team)
- [JIRA](https://jira.skatelescope.org/secure/RapidBoard.jspa?rapidView=449&projectKey=TOP&view=planning.nodetail&issueLimit=100)
